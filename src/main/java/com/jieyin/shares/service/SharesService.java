package com.jieyin.shares.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import com.jieyin.shares.pojo.Shares;
import com.jieyin.shares.service.BaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
public interface SharesService extends BaseService<Shares> {

    public Integer addShares(String sharesCode,String sharesName,Double limited,Integer mark,String description);

    public Integer deleteShares(Integer id);

    public Integer updateShares(Integer id ,String sharesCode,String sharesName,Double limited,Integer mark,String description);

    public Shares selectShares(Integer id);

    public List<Shares> selectAll();

    public List<String> distinct();

    public List<Shares> selectBycodeOrNameLike(String code);

    public List<Shares> selectByTs(String s);

    public List<Shares> selectHigh(Double high,String sharesCode);

    public List<Shares> selectLow(Double low,String sharesCode);

    public Page<Shares> findByPage(Integer pageNum, Integer pageSize, String codeOrName);
}
