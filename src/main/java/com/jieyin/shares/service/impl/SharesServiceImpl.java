package com.jieyin.shares.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jieyin.shares.pojo.Shares;
import com.jieyin.shares.mapper.SharesMapper;
import com.jieyin.shares.service.SharesService;
import com.jieyin.shares.utils.PageHelpers;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
@Service
public class SharesServiceImpl extends BaseServiceImpl<SharesMapper, Shares> implements SharesService {

    @Resource
    private SharesMapper sharesMapper;

    @Override
    public Integer addShares(String sharesCode,String sharesName,Double limited,Integer mark,String description) {
        Shares shares = new Shares();
        shares.setSharesCode(sharesCode);
        shares.setSharesName(sharesName);
        shares.setLimited(limited);
        shares.setMark(mark);
        shares.setDescription(description);
        shares.setCreateTime(LocalDateTime.now());
        int insert = sharesMapper.insert(shares);
        Integer integer = sharesMapper.selectLastId();
        if(insert>0){
            return integer;
        }else {
            return insert;
        }
    }

    @Override
    public Integer deleteShares(Integer id) {
        int i = sharesMapper.deleteById(id);
        return i;
    }

    @Override
    public Integer updateShares(Integer id ,String sharesCode,String sharesName,Double limited,Integer mark,String description) {
        Shares shares = new Shares();
        shares.setId(id);
        shares.setSharesCode(sharesCode);
        shares.setSharesName(sharesName);
        shares.setLimited(limited);
        shares.setMark(mark);
        shares.setDescription(description);
        shares.setUpdateTime(LocalDateTime.now());
        int i = sharesMapper.updateById(shares);
        return i;
    }

    @Override
    public Shares selectShares(Integer id) {
        QueryWrapper<Shares> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("id",id);
        Shares shares = sharesMapper.selectOne(objectQueryWrapper);
        return shares;
    }

    @Override
    public List<Shares> selectAll() {
        List<Shares> shares = sharesMapper.selectList(null);
        return shares;
    }

    @Override
    public List<String> distinct() {
        List<String> distinct = sharesMapper.distinct();
        return distinct;
    }

    @Override
    public List<Shares> selectBycodeOrNameLike(String code) {
        List<Shares> shares = sharesMapper.selectBycodeOrNameLike(code);
        return shares;
    }

    @Override
    public List<Shares> selectByTs(String code) {
        QueryWrapper<Shares> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("shares_code",code);
        List<Shares> shares = sharesMapper.selectList(objectQueryWrapper);
        return shares;
    }

    @Override
    public List<Shares> selectHigh(Double high,String sharesCode) {
        QueryWrapper<Shares> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("mark",0);
        objectQueryWrapper.eq("shares_code",sharesCode);
        objectQueryWrapper.le("limited",high);
        List<Shares> shares = sharesMapper.selectList(objectQueryWrapper);
        return shares;
    }

    @Override
    public List<Shares> selectLow(Double low,String sharesCode) {
        QueryWrapper<Shares> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("mark",1);
        objectQueryWrapper.eq("shares_code",sharesCode);
        objectQueryWrapper.ge("limited",low);
        List<Shares> shares = sharesMapper.selectList(objectQueryWrapper);
        return shares;
    }

    @Override
    public Page<Shares> findByPage(Integer pageNum, Integer pageSize,String codeOrName) {
        Page<Shares> page = PageHelpers.getPage(pageNum, pageSize);
        QueryWrapper<Shares> objectQueryWrapper = new QueryWrapper<>();
        if(codeOrName!=null && !"".equals(codeOrName)){
            objectQueryWrapper.like("shares_code",codeOrName).or().like("shares_name",codeOrName);
        }
        objectQueryWrapper.orderByDesc("create_time");
        Page<Shares> resultStockPage = page(page,objectQueryWrapper);
        return resultStockPage;
    }
}
