package com.jieyin.shares.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jieyin.shares.mapper.ResultMapper;
import com.jieyin.shares.pojo.ResultStock;
import com.jieyin.shares.pojo.Shares;
import com.jieyin.shares.pojo.bo.PageResult;
import com.jieyin.shares.service.ResultService;
import com.jieyin.shares.utils.PageHelpers;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
@Service
public class ResultServiceImpl extends BaseServiceImpl<ResultMapper, ResultStock> implements ResultService {

    @Resource
    private ResultMapper resultMapper;
    /**
     * 添加
     *
     * @param result
     * @return
     */
    @Override
    public Integer add(ResultStock result) {
        int insert = resultMapper.insert(result);
        return insert;
    }

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    @Override
    public ResultStock findById(Integer id) {
        ResultStock result = resultMapper.selectById(id);
        return result;
    }

    @Override
    public Integer deleteById(Integer id) {
        return null;
    }

    @Override
    public boolean removeById(Serializable id) {
        return false;
    }

    @Override
    public List<ResultStock> selectByCodeOrNameLike(String code) {
        List<ResultStock> resultStocks = resultMapper.selectByCodeOrNameLike(code);
        return resultStocks;
    }


    /**
     * 根据股票代码查询结果
     *
     * @param
     * @return
     */
    @Override
    public ResultStock selectByTS(String code) {
        QueryWrapper<ResultStock> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("shares_code",code);
        ResultStock result = resultMapper.selectOne(objectQueryWrapper);
        return result;
    }

    /**
     * 用户输入股票代码
     * @param s
     * @return
     */
    @Override
    public List<ResultStock> selectByCondition(String s) {
        List<ResultStock> results = resultMapper.selectByCondition(s);
        return results;
    }

    @Override
    public List<ResultStock> selectByDate(String date) {
        List<ResultStock> results = resultMapper.selectByDate(date);
        return results;
    }

    @Override
    public Page<ResultStock> findPage(String codeOrName, Integer pageNum, Integer pageSize) {
        Page<ResultStock> page = PageHelpers.getPage(pageNum, pageSize);
        QueryWrapper<ResultStock> objectQueryWrapper = new QueryWrapper<>();
        if(codeOrName!=null && !"".equals(codeOrName)){
            objectQueryWrapper.like("shares_code",codeOrName).or().like("shares_name",codeOrName);
        }
        objectQueryWrapper.orderByDesc("create_time");
        Page<ResultStock> resultStockPage = page(page,objectQueryWrapper);
        return resultStockPage;
    }
}
