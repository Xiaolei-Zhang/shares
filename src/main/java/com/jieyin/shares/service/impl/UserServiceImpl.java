package com.jieyin.shares.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jieyin.shares.pojo.User;
import com.jieyin.shares.mapper.UserMapper;
import com.jieyin.shares.service.UserService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public User selectByName(String userName,String passWord) {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("user_name",userName);
        userQueryWrapper.eq("pass_word",passWord);
        User user = userMapper.selectOne(userQueryWrapper);
        return user;
    }

    /**
     * 添加
     *
     * @param
     * @return
     */
    @Override
    public int add(String userName,String passWord) {
        User user = new User();
        user.setUserName(userName);
        user.setPassWord(passWord);
        int insert = userMapper.insert(user);
        return insert;
    }

    /**
     * 修改
     *
     * @param user
     * @return
     */
    @Override
    public int update(User user) {
        int i = userMapper.updateById(user);
        return i;
    }
}
