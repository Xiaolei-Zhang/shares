package com.jieyin.shares.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jieyin.shares.pojo.Basic;
import com.jieyin.shares.mapper.BasicMapper;
import com.jieyin.shares.service.BasicService;
import com.jieyin.shares.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
@Service
public class BasicServiceImpl extends BaseServiceImpl<BasicMapper, Basic> implements BasicService {

    @Resource
    private BasicMapper basicMapper;

    @Override
    public Basic selectByTS(String code) {
        QueryWrapper<Basic> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("ts_code", code);
        Basic basic = basicMapper.selectOne(objectQueryWrapper);
        return basic;
    }

    @Override
    public List<Basic> selectByCodeLike(String code) {
        List<Basic> basics = basicMapper.selectByCodeLike(code);
        return basics;
    }
}
