package com.jieyin.shares.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jieyin.shares.service.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements BaseService<T> {
    protected Logger log = LoggerFactory.getLogger(getClass());
}
