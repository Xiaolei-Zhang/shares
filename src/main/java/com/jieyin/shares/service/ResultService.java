package com.jieyin.shares.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import com.jieyin.shares.pojo.Basic;
import com.jieyin.shares.pojo.ResultStock;
import com.jieyin.shares.pojo.bo.PageResult;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
public interface ResultService extends BaseService<ResultStock> {

    /**
     * 添加
     * @param result
     * @return
     */
    public Integer add(ResultStock result);
    /**
     * 根据ID查询用户
     * @param id
     * @return
     */
    public ResultStock findById(Integer id);

    public Integer deleteById(Integer id);

    public List<ResultStock> selectByCodeOrNameLike(String code);

    /**
     * 根据股票代码查询结果
     * @param
     * @return
     */
    public ResultStock selectByTS(String code);

    public List<ResultStock> selectByCondition(String s);

    public List<ResultStock> selectByDate(String date);

    public Page<ResultStock> findPage(String s, Integer pageNum, Integer pageSize);
}
