package com.jieyin.shares.service;

import com.jieyin.shares.pojo.User;
import com.jieyin.shares.service.BaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
public interface UserService extends BaseService<User> {

    public User selectByName(String userName,String passWord);


    /**
     * 添加
     * @param
     * @return
     */
    public int add(String userName,String passWord);

    /**
     * 修改
     * @param user
     * @return
     */
    public int update(User user);


}
