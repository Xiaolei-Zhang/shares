package com.jieyin.shares.service.feign;

/**
 * feign调用示例，需要在Application中加入@EnableFeignClients注解
 * 调用不同的服务可通过多个service完成，单个service调用一个服务
 * 如不需要打印feign的http 请求日志，可以去除@FeignClient注解中的configuration = FeignConfiguration.class属性，也可以改为其他配置类
 */
//调用Eureka中的微服务可使用此注释，value为微服务中的Application值，可在http://www.touyue8.com:8761/中查询
//@FeignClient(value = "XXX", configuration = FeignConfiguration.class)
//调用非Eureka中的接口可使用此注释，修改对应的url值
//@FeignClient(name = "wechat", url = "https://api.weixin.qq.com/", configuration = FeignConfiguration.class)
public interface FeignService {
    //    调用请求参考如下，返回的Object可直接用Bean接收
    //        @PostMapping("/xxx")
    //        Object getResult(@RequestParam String xxx);
}
