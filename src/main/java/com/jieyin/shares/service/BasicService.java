package com.jieyin.shares.service;

import com.jieyin.shares.pojo.Basic;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
public interface BasicService extends BaseService<Basic> {
    public Basic selectByTS(String code);

    public List<Basic> selectByCodeLike(String code);
}
