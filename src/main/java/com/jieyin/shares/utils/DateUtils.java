package com.jieyin.shares.utils;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class DateUtils {

    /**
     * 时间格式(yyyyMMdd)
     */
    public final static String DATE_PATTERN = "yyyyMMdd";

    /**
     * 时间格式(yyyyMMdd)
     */
    public final static String DATE_SEND = "yyyy-MM-dd HH:mm:ss";


    /**
     * 对日期的【天】进行加/减
     *
     * @param date 日期
     * @param days 天数，负数为减
     * @return 加/减几天后的日期
     */
    public static Date addDateDays(Date date, int days) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusDays(days).toDate();
    }

    public static String selectDate() {
        Date date = new Date();
        DateTime dateTime = new DateTime(date);
        return format(dateTime.plusDays(0).toDate());
    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        for (String item : list) {
            if ("2".equals(item)) {
                list.remove(item);
            }
        }
    }
    /**
     * 日期格式化 日期格式为：yyyy-MM-dd
     *
     * @param date 日期
     * @return 返回yyyyMMdd格式日期
     */
    public static String format(Date date) {
        return format(date, DATE_SEND);
    }

    /**
     * 日期格式化 日期格式为：yyyyMMdd
     *
     * @param date 日期
     * @return 返回yyyyMMdd格式日期
     */
    public static String formatsend(Date date) {
        return format(date, DATE_SEND);
    }

    /**
     * 日期格式化 日期格式为：yyyy-MM-dd
     *
     * @param date    日期
     * @param pattern 格式，如：DateUtils.DATE_TIME_PATTERN
     * @return 返回yyyy-MM-dd格式日期
     */
    public static String format(Date date, String pattern) {
        if (date != null) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            return df.format(date);
        }
        return null;
    }

}
