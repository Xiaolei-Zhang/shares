package com.jieyin.shares.utils;

import org.springframework.http.HttpStatus;

public class LogicException extends RuntimeException {

    private HttpStatus status = HttpStatus.OK;

    private static final long serialVersionUID = 1L;

    public LogicException() {
        super();
    }

    public LogicException(HttpStatus status, String msg) {
        super(msg);
        this.status = status;
    }

    public LogicException(String msg) {
        super(msg);
    }

    public LogicException(Throwable t) {
        super(t);
    }

    public LogicException(String msg, Throwable t) {
        super(msg, t);
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
