package com.jieyin.shares.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class JsonUtils {
    private static Logger logger = LoggerFactory.getLogger(JsonUtils.class);
    private static Gson gson;

    public static Gson getGson() {
        if (gson == null) {
            synchronized (JsonUtils.class) {
                if (gson == null) {
                    gson = new GsonBuilder()
                            .disableHtmlEscaping()//加入此行后Html特殊字符如 <,>,',= 等将不会转义，保持原有字符。需要根据实际情况判断是否按此处理
                            .registerTypeAdapter(Map.class, (JsonDeserializer<Map<String, Object>>) (json, typeOfT, context) -> {
                                Map<String, Object> map = new HashMap<>();
                                JsonObject jsonObject = json.getAsJsonObject();
                                Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
                                for (Map.Entry<String, JsonElement> entry : entrySet) {
                                    map.put(entry.getKey(), entry.getValue());
                                }
                                return map;
                            })
                            .setDateFormat("yyyy-MM-dd HH:mm:ss")
                            .create();
                }
            }
        }
        return gson;
    }

    public static String toJson(Object o) {
        return getGson().toJson(o);
    }

    public static <T> T fromJson(String json, Class<T> tClass) {
        return getGson().fromJson(json, tClass);
    }

    public static <T> T fromJson(String json, Type type) {
        return getGson().fromJson(json, type);
    }


}
