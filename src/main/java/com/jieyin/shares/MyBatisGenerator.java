package com.jieyin.shares;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.jieyin.shares.controller.BaseController;
import com.jieyin.shares.mapper.MapperBase;
import com.jieyin.shares.pojo.BasePojo;
import com.jieyin.shares.service.BaseService;
import com.jieyin.shares.service.impl.BaseServiceImpl;

/**
 * MyBatisPlus代码生成器
 */
public class MyBatisGenerator {
    private static String packageName = MyBatisGenerator.class.getPackage().getName();

    public static void main(String[] args) {
        //user -> UserService, 设置成true: user -> IUserService
        //TODO 修改为代码生成后存放的位置
        String dir = "E:/Desktop/1";
        generateByTables(false, packageName, dir);
    }

    private static void generateByTables(String packageName, String dir) {
        generateByTables(false, packageName, dir);
    }

    private static void generateByTables(boolean serviceNameStartWithI, String packageName, String dir) {
        AutoGenerator mpg = new AutoGenerator();

        GlobalConfig globalConfig = new GlobalConfig();
        // 全局配置
        globalConfig.setActiveRecord(true)   //是否支持AR模式
                .setAuthor("xiaolei")   // 设置作者
                .setOutputDir(dir)   // 设置生成的目标路径（绝对路径）
                .setIdType(IdType.AUTO) //主键策略
                .setBaseColumnList(true)  // 设置sql片段
                .setBaseResultMap(true)  // resultMap
                .setEnableCache(false)   // 不开缓存
                .setServiceName("%sService")
                .setServiceImplName("%sServiceImpl")
                .setMapperName("%sMapper")
                .setXmlName("%sMapper")
                .setSwagger2(false) //实体属性 Swagger2 注解
                .setOpen(false) //生成之后 默认打开文件夹
                .setFileOverride(true);  // 每一次生成需要覆盖
        if (serviceNameStartWithI) {
            globalConfig.setServiceName("I%sService");
        }

        //TODO 修改为对应项目的数据库连接信息
        //mysql
        String dbUrl = "jdbc:mysql://localhost:3306/shares?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&failOverReadOnly=false&maxReconnects=15000&allowMultiQueries=true&useSSL=false&serverTimezone=Asia/Shanghai";
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL)
                .setUrl(dbUrl)
                .setUsername("root")
                .setPassword("123456")
                .setDriverName("com.mysql.cj.jdbc.Driver");

        // 策略配置
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig
                //排除哪些表不生成//null == 默认全部生成
                //.setExclude(null)
                //生成哪些表  多个传数组
                //.setInclude(tableNames)
                .setCapitalMode(false)   // 全局大写命名
                .setEntityLombokModel(true)   // Lomnok
                // .setDbColumnUnderline(true)   // 表名 字段名 是否使用下滑线命名
                //.setTablePrefix("tb_") // 去除表前缀
                .setRestControllerStyle(true)
                .setNaming(NamingStrategy.underline_to_camel) //// underline_to_camel数据库表映射到实体的命名策略
                //设置父类路径
                //                .setSuperMapperClass(packageName + ".mapper.BaseMapper")
                //                .setSuperServiceClass(packageName + ".service.BaseService")
                //                .setSuperServiceImplClass(packageName + ".service.impl.BaseServiceImpl")
                .setSuperServiceClass(BaseService.class)
                .setSuperServiceImplClass(BaseServiceImpl.class)
                .setSuperControllerClass(BaseController.class)
                .setSuperMapperClass(MapperBase.class.getName())
                .setSuperEntityClass(BasePojo.class)
                .setSuperEntityColumns("create_time", "update_time");

        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent(packageName)
                .setController("controller")
                .setEntity("pojo");

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        mpg.setDataSource(dataSourceConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(packageConfig)
                .setGlobalConfig(globalConfig)
                .setCfg(cfg)
                .execute();
    }

}
