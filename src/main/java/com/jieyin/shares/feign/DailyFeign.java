package com.jieyin.shares.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "daily-client", url = "http://api.tushare.pro")
public interface DailyFeign {

    @PostMapping()
    String searchRepo(String q);
}
