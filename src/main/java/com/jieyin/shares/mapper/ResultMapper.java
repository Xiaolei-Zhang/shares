package com.jieyin.shares.mapper;

import com.jieyin.shares.pojo.ResultStock;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
public interface ResultMapper extends MapperBase<ResultStock> {

    public ResultStock selectByTS(String s);

    public List<ResultStock> selectByCondition(String s);

    public List<ResultStock> selectByDate(String date);

    @Select("select * from result_stock where shares_code like '%${code}%' or shares_name like '%${code}%' ")
    public List<ResultStock> selectByCodeOrNameLike(String code);



}
