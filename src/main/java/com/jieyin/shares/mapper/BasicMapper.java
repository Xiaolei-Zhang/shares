package com.jieyin.shares.mapper;

import com.jieyin.shares.pojo.Basic;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
public interface BasicMapper extends MapperBase<Basic> {

    @Select("select * from basic where ts_code like '%${code}%'")
    public List<Basic> selectByCodeLike(String code);
}
