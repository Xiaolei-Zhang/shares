package com.jieyin.shares.mapper;

import com.jieyin.shares.pojo.Basic;
import com.jieyin.shares.pojo.Shares;
import com.jieyin.shares.mapper.MapperBase;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
public interface SharesMapper extends MapperBase<Shares> {

    public List<String> distinct();

    @Select("select max(id) from shares")
    public Integer selectLastId();

    @Select("select * from shares where shares_code like '%${code}%' or shares_name like '%${code}%'")
    public List<Shares> selectBycodeOrNameLike(String code);
}
