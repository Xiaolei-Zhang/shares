package com.jieyin.shares.mapper;

import com.jieyin.shares.pojo.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
public interface UserMapper extends MapperBase<User> {
}
