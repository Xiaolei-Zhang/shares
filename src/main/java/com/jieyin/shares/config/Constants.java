package com.jieyin.shares.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

public class Constants {

    public class ConfigData {
        //TODO 需要改为对应项目的mapper所在包名
        public static final String MAPPER_PACKAGE = "com.jieyin.shares.mapper";
    }

    public class HttpHeader {
        public static final String KEY_ACCESS_TOKEN = "Authorization";
    }

    @Component
    public static class Profile {
        public static String businessCode;

        @Value("${businessCode}")
        public void setBusinessCode(String businessCode) {
            Profile.businessCode = businessCode;
        }
    }


}
