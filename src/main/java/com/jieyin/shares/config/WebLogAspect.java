package com.jieyin.shares.config;

import com.jieyin.shares.utils.JsonUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.util.Enumeration;

@Aspect
@Component
public class WebLogAspect {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 处理请求controller层日志输出
     */
    @Pointcut("@annotation(org.springframework.web.bind.annotation.DeleteMapping)" +
            "||@annotation(org.springframework.web.bind.annotation.GetMapping)" +
            "||@annotation(org.springframework.web.bind.annotation.PatchMapping)" +
            "||@annotation(org.springframework.web.bind.annotation.PostMapping)" +
            "||@annotation(org.springframework.web.bind.annotation.PutMapping)" +
            "||@annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void webLog() {
    }

    /**
     * 在切点之前织入
     *
     * @param joinPoint
     * @throws Throwable
     */
    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        // 开始打印请求日志
        joinPoint.getThis();
        //        if (null != joinPoint.getArgs()[0]){
        //            logger.info("o   :{}", JSONObject.toJSONString(joinPoint.getArgs()[0]));
        //        }
        joinPoint.getTarget();
        //用的最多 通知的签名
        Signature signature = joinPoint.getSignature();
        //AOP代理类的类（class）信息
        signature.getDeclaringType();
        //获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        //Feign开启Hystrix支持后，默认隔离策略是THREAD 。而 RequestContextHolder 源码中，使用了两个血淋淋的ThreadLocal 。导致requestAttributes为null。目前feign日志打印通过FeignConfiguration类，暂时只在此做个非空校验。若需要在此处理，需要自定义并发策略。
        if (requestAttributes != null) {
            //从获取RequestAttributes中获取HttpServletRequest的信息
            HttpServletRequest request = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);

            // 打印请求相关参数
            logger.info("========================================== Start ==========================================");
            // 打印请求 url
            logger.info("URL            : {}", request.getRequestURL().toString());
            // 打印 Http method
            logger.info("HTTP Method    : {}", request.getMethod());
            // 打印调用 controller 的全路径以及执行方法
            logger.info("Class Method   : {}.{}", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
            // 打印请求的 IP
            logger.info("IP             : {}", request.getRemoteAddr());
            // 打印header
            logger.info("===================================== Request Headers =====================================");
            Enumeration<String> headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String name = headerNames.nextElement();
                //根据名称获取请求头的值
                String value = request.getHeader(name);
                logger.info(name + ":" + value);
            }
            logger.info("===========================================================================================");
            // 打印请求入参
            logger.info("====================================== Request Params =====================================");
            if (request.getQueryString() != null) {
                logger.info("QueryString   : {}", URLDecoder.decode(request.getQueryString(), "UTF-8"));
            }
            logger.info("ParamsMap     : {}", JsonUtils.toJson(request.getParameterMap()));
            try {
                logger.info("================================ Body ================================  ");
                Object[] objects = joinPoint.getArgs();
                for (Object o : objects) {
                    if (o instanceof HttpServletRequest || o instanceof HttpServletResponse) {
                        continue;
                    }
                    logger.info(JsonUtils.toJson(o));
                }
                logger.info("============================== Body end ============================== ");
            } catch (Throwable t) {
                logger.error("cannot print body.reason:{}", t.getMessage());
                t.printStackTrace();
            }
            logger.info("===========================================================================================");
        } else {
            logger.info("========================================== Start ==========================================");
            logger.info("requestAttributes is null");
        }
    }

    /**
     * 在切点之后织入
     *
     * @throws Throwable
     */
    @After("webLog()")
    public void doAfter() throws Throwable {

    }

    /**
     * 环绕
     *
     * @param proceedingJoinPoint
     * @return
     * @throws Throwable
     */
    @Around("webLog()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();

        Object result = proceedingJoinPoint.proceed();
        // 打印出参
        if (result instanceof String) {
            logger.info("Response Args  : {}", result);
        } else {
            logger.info("Response Args  : {}", JsonUtils.toJson(result));
        }
        // 执行耗时
        logger.info("Time-Consuming : {} ms", System.currentTimeMillis() - startTime);
        logger.info("=========================================== End ===========================================");
        // 每个请求之间空一行
        logger.info("");
        return result;
    }

}