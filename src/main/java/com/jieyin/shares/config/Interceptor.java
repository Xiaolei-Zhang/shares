package com.jieyin.shares.config;

import com.jieyin.shares.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Interceptor implements HandlerInterceptor {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestUrl = request.getRequestURL().toString();
        String contextPath = request.getContextPath();
        String target = requestUrl.substring(requestUrl.indexOf(contextPath) + contextPath.length());
        logger.info(target);
        logger.info(JsonUtils.toJson(request.getParameterMap()));
        return true;
    }

}
