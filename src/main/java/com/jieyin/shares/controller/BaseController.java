package com.jieyin.shares.controller;

import com.jieyin.shares.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

public class BaseController {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 根据操作结果返回通用结果
     *
     * @param flag 操作结果
     * @return 通用输出结果
     */
    protected ResponseEntity<?> getResponseByResult(boolean flag) {
        return getResponseByResult(flag, "操作失败");
    }

    /**
     * 根据操作结果返回通用结果
     *
     * @param flag         操作结果
     * @param errorMessage 提示信息
     * @return 通用输出结果
     */
    protected ResponseEntity<?> getResponseByResult(boolean flag, String errorMessage) {
        if (flag) {
            return ResultUtil.success().toResponseEntity();
        } else {
            return ResultUtil.error(errorMessage).toResponseEntity();
        }
    }

    /**
     * 判断值是否是允许值范围内
     *
     * @param value  值
     * @param allows 允许值
     * @return 是否在允许范围内
     */
    protected boolean allowValue(Object value, Object... allows) {
        for (Object o : allows) {
            if (Objects.equals(o, value)) {
                return true;
            }
        }
        return false;
    }
}
