package com.jieyin.shares.controller;


import com.jieyin.shares.pojo.Basic;
import com.jieyin.shares.pojo.bo.Result;
import com.jieyin.shares.service.BasicService;
import com.jieyin.shares.utils.ResultUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
@CrossOrigin
@RestController
@RequestMapping("/basic")
public class BasicController extends BaseController {

    @Resource
    private BasicService basicService;

    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "股票代码" ,dataType = "String"),
    })
    @ApiOperation("用户输入股票代码进行提示")
    @PostMapping("/bylikecode")
    public ResponseEntity<Result> selectByLike(String code){
        List<Basic> list = basicService.selectByCodeLike(code);
        if(list.size()>0){
            Result<List<Basic>> success = ResultUtil.success(list);
            return success.toResponseEntity(HttpStatus.OK);
        }else {
            Result<String> error = ResultUtil.error("未查到股票信息");
            return error.toResponseEntity();
        }
    }
}

