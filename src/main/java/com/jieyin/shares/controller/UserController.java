package com.jieyin.shares.controller;


import com.jieyin.shares.pojo.User;
import com.jieyin.shares.pojo.bo.Result;
import com.jieyin.shares.service.UserService;
import com.jieyin.shares.utils.ResultUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {
    @Resource
    private UserService userService;

    @ApiOperation("用户登录验证")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名"),
            @ApiImplicitParam(name = "password", value = "密码"),
    })
    @PostMapping("/login")
    public ResponseEntity<Result> login(String username, String password){
        User user = userService.selectByName(username,password);
        if(user != null){
            return ResultUtil.success("登录成功!").toResponseEntity(HttpStatus.OK);
        }else {
            return ResultUtil.error("账号或密码错误!").toResponseEntity();
        }
    }

    /**
     * 增加用户
     * @param
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名" ,dataType = "String"),
            @ApiImplicitParam(name = "password", value = "密码",dataType = "String"),
    })
    @ApiOperation("添加用户信息")
    @PostMapping("/add")
    public ResponseEntity<Result> addUser(String username, String password){
        int add = userService.add(username,password);
        if(add>0){
            return ResultUtil.success(add).toResponseEntity(HttpStatus.OK);
        }else {
            return ResultUtil.error("添加失败").toResponseEntity();
        }

    }

    /**
     * 更新用户信息
     * @param
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名" ,dataType = "String"),
            @ApiImplicitParam(name = "password", value = "密码",dataType = "String"),
    })
    @ApiOperation("用户信息更新")
    @PostMapping("/update")
    public ResponseEntity<Result> doUpdate(String username, String password) {
        User user = new User();
        user.setUserName(username);
        user.setPassWord(password);
        int update = userService.update(user);
        if(update>0){
            return ResultUtil.success(update).toResponseEntity(HttpStatus.OK);
        }else {
            return ResultUtil.error("更新失败").toResponseEntity();
        }
    }
}

