package com.jieyin.shares.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jieyin.shares.feign.DailyFeign;
import com.jieyin.shares.pojo.Basic;
import com.jieyin.shares.pojo.ResultStock;
import com.jieyin.shares.pojo.SendParam;
import com.jieyin.shares.pojo.Shares;
import com.jieyin.shares.pojo.bo.Result;
import com.jieyin.shares.service.BasicService;
import com.jieyin.shares.service.ResultService;
import com.jieyin.shares.service.SharesService;
import com.jieyin.shares.utils.DateUtils;
import com.jieyin.shares.utils.HttpUtils;
import com.jieyin.shares.utils.ResultUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {

    @Resource
    private SharesService sharesService;

    @Resource
    private BasicService stockBasicService;

    @Resource
    private ResultService resultService;

    @Resource
    private DailyFeign dailyFeign;

    @ApiOperation("查询股票信息，存入数据库")
    @PostMapping("/select")
    public Result<String> querystock() {
        //查出共有多少种股票
        List<String> distinct = sharesService.distinct();
        ArrayList<String> list = new ArrayList<>();
        SendParam sendParam = new SendParam();

        //拼接要查询的股票代码用以查询daily
        for (String share : distinct) {
            list.add(share);
            System.out.println(share);
        }
        sendParam.setTsCode(list);
        sendParam.setTradeDate("20220321");
        //sendParam.setTradeDate(DateUtils.format(new Date()));

        System.out.println(sendParam);
        //调用方法获取信息,用户添加的不同的股票
        String send = HttpUtils.sendParam(sendParam);
        String s = dailyFeign.searchRepo(send);
        System.out.println("recive:" + s);
        String data = JSONObject.parseObject(s).getString("data");
        JSONArray items = JSONObject.parseObject(data).getJSONArray("items");
        String format = DateUtils.format(new Date());
        //"ts_code",0  "trade_date"1   "open"2  "high"3  "low"4   "close"5
        //解析每个股票的信息
        if (items.size() > 0) {
            for (Object arr : items) {
                List<String> daily = JSONObject.parseArray(arr.toString(), String.class);
                Basic basic = stockBasicService.selectByTS(daily.get(0));
                //查出所有标记为上限的股票
                List<Shares> sharesHigh = sharesService.selectHigh(Double.parseDouble(daily.get(3)), daily.get(0));
                if (sharesHigh.size() > 0) {
                    for (Shares high : sharesHigh) {
                        //对用户定义的上限值与股票日常信息进行对比，超过的话就记录该上限值  daily.get(5)收盘价
                        ResultStock result = new ResultStock(daily.get(0), basic.getName(), high.getLimited(), 0, high.getDescription(), daily.get(5), format, Double.parseDouble(daily.get(3)), LocalDateTime.now());
                        resultService.add(result);
                        log.info("最高价超过上限，已记录");
                    }
                }

                List<Shares> sharesLow = sharesService.selectLow(Double.parseDouble(daily.get(4)), daily.get(0));
                if (sharesLow.size() > 0) {
                    for (Shares low : sharesLow) {
                        //对用户定义的下限值与股票日常信息进行对比，低于的话就记录该上限值
                        ResultStock result = new ResultStock(daily.get(0), basic.getName(), low.getLimited(), 1, low.getDescription(), daily.get(5), format, Double.parseDouble(daily.get(4)), LocalDateTime.now());
                        resultService.add(result);
                        log.info("最低价低于下限，已记录");
                    }
                }
            }
        }

        return ResultUtil.success(s);
    }
}
