package com.jieyin.shares.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jieyin.shares.pojo.ResultStock;
import com.jieyin.shares.pojo.bo.Result;
import com.jieyin.shares.service.ResultService;
import com.jieyin.shares.utils.ResultUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
@Slf4j
@RestController
@RequestMapping("/result")
public class ResultController extends BaseController {

    @Resource
    private ResultService resultService;


    @ApiImplicitParams({
            @ApiImplicitParam(name = "codeOrName", value = "每页有几条数据", dataType = "String"),
            @ApiImplicitParam(name = "pageNum", value = "要查询第几页", dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页有几条数据", dataType = "Integer"),
    })
    @ApiOperation("条件分页查询")
    @PostMapping("/findByPage")
    public ResponseEntity<Result> findByPage(String codeOrName, Integer pageNum, Integer pageSize) {
        Page<ResultStock> page = resultService.findPage(codeOrName, pageNum, pageSize);
        if (page != null) {
            return ResultUtil.success(page).toResponseEntity(HttpStatus.OK);
        } else {
            return ResultUtil.error("暂未查到数据！").toResponseEntity();
        }
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要删除的数据id集合", dataType = "String"),
    })
    @ApiOperation("结果信息信息-批量删除")
    @PostMapping("/deleteIds")
    public ResponseEntity<Result> deleteIds(String ids) {
        System.out.println(ids);
        List<Integer> integers = JSONObject.parseArray(ids, Integer.class);
        if (integers.size() > 0) {
            boolean b = resultService.removeByIds(integers);
            if (b) {
                return ResultUtil.success("已删除" + integers.size() + "条数据").toResponseEntity(HttpStatus.OK);
            } else {
                return ResultUtil.error("删除失败！").toResponseEntity();
            }
        } else {
            return ResultUtil.error("删除零条数据").toResponseEntity();
        }
    }

}

