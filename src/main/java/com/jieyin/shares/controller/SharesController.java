package com.jieyin.shares.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jieyin.shares.pojo.Shares;
import com.jieyin.shares.pojo.bo.Result;
import com.jieyin.shares.service.SharesService;
import com.jieyin.shares.utils.ResultUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
@RestController
@RequestMapping("/shares")
public class SharesController extends BaseController {

    @Resource
    private SharesService sharesService;

    /**
     * 增加用户
     * @param
     * @return id
     */
    @ApiOperation("用户自定义股票信息-添加")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sharesCode", value = "股票代码",dataType = "String"),
            @ApiImplicitParam(name = "sharesName", value = "股票名称",dataType = "String"),
            @ApiImplicitParam(name = "limited", value = "上下限值", dataType = "Double"),
            @ApiImplicitParam(name = "mark", value = "上下限",dataType = "Integer"),
            @ApiImplicitParam(name = "description", value = "描述",dataType = "String"),
    })
    @PostMapping("/add")
    public ResponseEntity<Result> addUser(String sharesCode, String sharesName, Double limited, Integer mark, String description){
        int add = sharesService.addShares(sharesCode,sharesName,limited,mark,description);
        if(add>0){
            return ResultUtil.success(add).toResponseEntity(HttpStatus.OK);
        }else {
            return ResultUtil.error("添加失败！").toResponseEntity();
        }
    }

    /**
     * 删除股票信息
     * @param id
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要删除的数据id",dataType = "Integer"),
    })
    @ApiOperation("用户自定义股票信息-删除")
    @PostMapping("/delete")
    public ResponseEntity<Result> delete(Integer id) {
        int delete = sharesService.deleteShares(id);
        if(delete>0){
            return ResultUtil.success(delete).toResponseEntity(HttpStatus.OK);
        }else {
            return ResultUtil.error("删除失败！").toResponseEntity();
        }
    }

    /**
     * 删除股票信息
     * @param
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要删除的数据id",dataType = "String"),
    })
    @ApiOperation("用户自定义股票信息-批量删除")
    @PostMapping("/deleteIds")
    public ResponseEntity<Result> deleteids(String ids) {
        System.out.println(ids);
        List<Integer> integers = JSONObject.parseArray(ids, Integer.class);
        Integer a = 0;
        for(Object id:integers){
            a += sharesService.deleteShares(Integer.parseInt(id.toString()));
        }
        if(a==integers.size()){
            return ResultUtil.success("已删除"+a+"条数据").toResponseEntity(HttpStatus.OK);
        }else {
            return ResultUtil.error("删除失败！").toResponseEntity();
        }

    }

    /**
     * 更新股票信息
     * @param
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要更新的数据id"),
            @ApiImplicitParam(name = "sharesCode", value = "股票代码",dataType = "String"),
            @ApiImplicitParam(name = "sharesName", value = "股票名称",dataType = "String"),
            @ApiImplicitParam(name = "limited", value = "上下限值", dataType = "Double"),
            @ApiImplicitParam(name = "mark", value = "上下限",dataType = "Integer"),
            @ApiImplicitParam(name = "description", value = "描述",dataType = "String"),
    })
    @ApiOperation("用户自定义股票信息-更新")
    @PostMapping("/update")
    public ResponseEntity<Result> doUpdate(Integer id ,String sharesCode,String sharesName,Double limited,Integer mark,String description) {
        int update = sharesService.updateShares(id,sharesCode,sharesName,limited,mark,description);
        if(update>0){
            return ResultUtil.success(update).toResponseEntity(HttpStatus.OK);
        }else {
            return ResultUtil.error("更新失败！").toResponseEntity();
        }
    }

    /**
     * 分页查询
     * @param
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "要查询第几页",dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页有几条数据",dataType = "Integer"),
            @ApiImplicitParam(name = "codeOrName", value = "查询条件",dataType = "String"),
    })
    @ApiOperation("用户自定义股票信息-条件分页查询")
    @PostMapping("select")
    public ResponseEntity<Result> selectbyid(Integer pageNum, Integer pageSize, String codeOrName){
        Page<Shares> byPage = sharesService.findByPage(pageNum, pageSize,codeOrName);
        if(byPage!=null){
            return ResultUtil.success(byPage).toResponseEntity(HttpStatus.OK);
        }else {
            return ResultUtil.error("暂未查到数据！").toResponseEntity();
        }
    }
}

