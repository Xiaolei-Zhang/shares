package com.jieyin.shares.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class User extends BasePojo<User> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String userName;

    private String passWord;



    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
