package com.jieyin.shares.pojo.bo;

import lombok.Data;

import java.util.List;

@Data
public class PageResult<T> {
    private List<T> list;
    private Long total;
    private Long pages;

}
