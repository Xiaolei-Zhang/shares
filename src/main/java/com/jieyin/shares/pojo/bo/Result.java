package com.jieyin.shares.pojo.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Data
public class Result<T> {

    /**
     * 状态码：0成功 1失败
     */
    private Integer code;

    /**
     * 提示信息.
     */
    private String msg;

    /**
     * 具体的内容.
     */
    private T data;

    @JsonIgnore
    public boolean isSuccess() {
        return code == 0;
    }

    public ResponseEntity<Result> toResponseEntity() {
        return toResponseEntity(HttpStatus.OK);
    }

    public ResponseEntity<Result> toResponseEntity(HttpStatus status) {
        return new ResponseEntity<>(this, status);
    }
}
