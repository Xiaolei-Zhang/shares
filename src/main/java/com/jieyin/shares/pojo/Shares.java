package com.jieyin.shares.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.jieyin.shares.pojo.BasePojo;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Shares extends BasePojo<Shares> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 股票代码
     */
    private String sharesCode;

    /**
     * 股票名称
     */
    private String sharesName;

    /**
     * 上下限
     */
    private Double limited;

    /**
     * 标志位
     *  0:上限
     *  1:下限
     */
    private Integer mark;

    /**
     * 用户自定义描述
     */
    private String description;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
