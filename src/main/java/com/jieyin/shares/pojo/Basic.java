package com.jieyin.shares.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.jieyin.shares.pojo.BasePojo;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiaolei
 * @since 2022-03-17
 */
@Data
public class Basic{

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String tsCode;

    private String symbol;

    private String name;

    private String area;

    private String industry;

    private String market;

    private String listDate;


    public Basic(String ts_code, String symbol, String name, String area, String industry, String market, String list_date) {
        this.tsCode = ts_code;
        this.symbol = symbol;
        this.name = name;
        this.area = area;
        this.industry = industry;
        this.market = market;
        this.listDate = list_date;
    }

    public Basic(Integer id, String ts_code, String symbol, String name, String area, String industry, String market, String list_date) {
        this.id = id;
        this.tsCode = ts_code;
        this.symbol = symbol;
        this.name = name;
        this.area = area;
        this.industry = industry;
        this.market = market;
        this.listDate = list_date;
    }

    public Basic() {
    }
}
