package com.jieyin.shares.pojo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 所有表都添加了的公用字段
 * <p>
 * 若某个表没有某字段，需要在对应实体类中定义对应属性，并添加transient关键字。
 * 例：
 * Test表中没有create_time字段，则需要在Test实体对象中加入下面的代码：
 * protected transient LocalDateTime createTime;
 * <p>
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BasePojo<T extends Model<T>> extends Model<T> {
    @JsonIgnore
    protected LocalDateTime createTime;

    @JsonIgnore
    protected LocalDateTime updateTime;
}
