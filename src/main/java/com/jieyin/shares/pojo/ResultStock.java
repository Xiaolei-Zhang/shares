package com.jieyin.shares.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.time.LocalDateTime;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResultStock extends BasePojo<ResultStock>{

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String sharesCode;

    private String sharesName;

    private Double limited;

    private Integer mark;

    private String description;

    private String stockClosed;

    private String addTime;

    private Double extremum;

    public ResultStock(String sharesCode, String sharesName, Double limited, Integer mark, String description, String stockClosed, String addTime, Double extremum, LocalDateTime createTime) {
        this.sharesCode = sharesCode;
        this.sharesName = sharesName;
        this.limited = limited;
        this.mark = mark;
        this.description = description;
        this.stockClosed = stockClosed;
        this.addTime = addTime;
        this.extremum = extremum;
        this.createTime = createTime;
    }

    public ResultStock() {
    }

}
