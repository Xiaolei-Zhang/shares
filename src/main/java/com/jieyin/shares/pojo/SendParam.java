package com.jieyin.shares.pojo;

import lombok.Data;

import java.util.List;

@Data
public class SendParam {
    private List<String> tsCode;

    private String tradeDate;

}
