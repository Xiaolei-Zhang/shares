package com.jieyin.shares.task;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jieyin.shares.feign.DailyFeign;
import com.jieyin.shares.pojo.Basic;
import com.jieyin.shares.pojo.ResultStock;
import com.jieyin.shares.pojo.SendParam;
import com.jieyin.shares.pojo.Shares;
import com.jieyin.shares.service.BasicService;
import com.jieyin.shares.service.ResultService;
import com.jieyin.shares.service.SharesService;
import com.jieyin.shares.utils.DateUtils;
import com.jieyin.shares.utils.HttpUtils;
import com.jieyin.shares.utils.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Slf4j
//@Component
public class ExtractFeature {

    @Resource
    private SharesService sharesService;

    @Resource
    private BasicService stockBasicService;

    @Resource
    private ResultService resultService;

    @Resource
    private DailyFeign dailyFeign;

    //TODO 需要更改
    //@Scheduled(cron = "0 0 23 * * ?")
    @Scheduled(cron = "0/10 * * * * ?")
    public void querystock(){
        //查出共有多少种股票
        List<String> distinct = sharesService.distinct();
        ArrayList<String> list = new ArrayList<>();
        SendParam sendParam = new SendParam();

        //拼接要查询的股票代码用以查询daily
        for (String share : distinct) {
            list.add(share);
            System.out.println(share);
        }
        sendParam.setTsCode(list);
        sendParam.setTradeDate("20220321");
        //sendParam.setTradeDate(DateUtils.format(new Date()));

        System.out.println(sendParam);
        //调用方法获取信息,用户添加的不同的股票
        String send = HttpUtils.sendParam(sendParam);
        String s = dailyFeign.searchRepo(send);
        System.out.println("recive:" + s);
        String data = JSONObject.parseObject(s).getString("data");
        JSONArray items = JSONObject.parseObject(data).getJSONArray("items");
        String format = DateUtils.format(new Date());
        //"ts_code",0  "trade_date"1   "open"2  "high"3  "low"4   "close"5
        //解析每个股票的信息
        if (items.size() > 0) {
            for (Object arr : items) {
                List<String> daily = JSONObject.parseArray(arr.toString(), String.class);
                Basic basic = stockBasicService.selectByTS(daily.get(0));
                //查出所有标记为上限的股票
                List<Shares> sharesHigh = sharesService.selectHigh(Double.parseDouble(daily.get(3)),daily.get(0));
                if (sharesHigh.size() > 0) {
                    for (Shares high : sharesHigh) {
                        //对用户定义的上限值与股票日常信息进行对比，超过的话就记录该上限值  daily.get(5)收盘价
                        ResultStock result = new ResultStock(daily.get(0), basic.getName(), high.getLimited(), 0, high.getDescription(), daily.get(5), format, Double.parseDouble(daily.get(3)), LocalDateTime.now());
                        resultService.add(result);
                        log.info("最高价超过上限，已记录");
                    }
                }

                List<Shares> sharesLow = sharesService.selectLow(Double.parseDouble(daily.get(4)),daily.get(0));
                if (sharesLow.size() > 0) {
                    for (Shares low : sharesLow) {
                        //对用户定义的下限值与股票日常信息进行对比，低于的话就记录该上限值
                        if (Double.parseDouble(daily.get(4)) <= low.getLimited()) {
                            ResultStock result = new ResultStock(daily.get(0), basic.getName(), low.getLimited(), 1, low.getDescription(), daily.get(5), format, Double.parseDouble(daily.get(4)), LocalDateTime.now());
                            resultService.add(result);
                            log.info("最低价低于下限，已记录");
                        }
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(date);
        System.out.println(date);

        LocalDateTime now = LocalDateTime.now();

    }
}
