package com.jieyin.shares.service.impl;

import com.github.pagehelper.PageInfo;
import com.jieyin.shares.mapper.ResultMapper;
import com.jieyin.shares.mapper.SharesMapper;
import com.jieyin.shares.pojo.ResultStock;
import com.jieyin.shares.pojo.Shares;
import com.jieyin.shares.pojo.User;
import com.jieyin.shares.pojo.bo.PageResult;
import com.jieyin.shares.service.ResultService;
import com.jieyin.shares.service.SharesService;
import com.jieyin.shares.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Resource
    private SharesMapper sharesMapper;

    @Resource
    private ResultMapper resultMapper;

    @Resource
    private SharesService sharesService;

    @Resource
    private ResultService resultService;

    @Test
    public void selectByName() {


    }

    @Test
    public void test(){
        List<String> distinct = sharesMapper.distinct();
        for(String s:distinct){
            System.out.println(s);
        }
    }

    @Test
    public void testt(){
//        ResultStock resultStock = resultMapper.selectById(1);
//        System.out.println(resultStock);
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<ResultStock> resultStocks = resultMapper.selectByDate(simpleDateFormat.format(date));
        System.out.println(resultStocks.get(0));
    }

    @Test
    public void test1(){
        ResultStock byId = resultMapper.selectById(1);
        System.out.println(byId);
    }

    @Test
    public void test2(){



    }

}